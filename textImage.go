package main

import (
	"fmt"

	"github.com/fogleman/gg"
)

type Request struct {
	// ImgPath  string
	FontPath string
	FontSize float64
	// Text     string
}

func main() {
	r := Request{
		FontPath: "arial.ttf",
		FontSize: 100,
	}
	var ImgPath string
	fmt.Print("Enter the Path of the Image: ")
	fmt.Scan(&ImgPath)

	buffer, err := gg.LoadImage(ImgPath)
	if err != nil {
		panic(err)
	}

	var Text string
	fmt.Print("Enter the text: ")
	fmt.Scan(&Text)

	// text := bufio.NewReader(os.Stdin)
	// Text, _ := text.ReadString('\n')

	ImgWidth := buffer.Bounds().Dx()
	ImgHeight := buffer.Bounds().Dy()

	dc := gg.NewContext(ImgWidth, ImgHeight)

	dc.DrawImage(buffer, 0, 0)

	if err := dc.LoadFontFace(r.FontPath, r.FontSize); err != nil {
		panic(err)
	}

	x := float64(ImgWidth / 2)
	y := float64((ImgHeight / 2) - 80)

	MaxWidth := float64(ImgWidth) - 30.0

	dc.SetHexColor("#000")
	dc.SetHexColor("#FFF")
	var pick int
	fmt.Println("Choose 1 to add text at bottom")
	fmt.Println("Choose 2 to add text at up")
	fmt.Println("Choose 3 to add text at left")
	fmt.Println("Choose 4 to add text at right")
	fmt.Println("Choose 5 to add text at centre")
	fmt.Scan(&pick)
	switch {
	case pick == 1:
		dc.DrawStringAnchored(Text, float64(ImgWidth)/2, float64(ImgHeight)-r.FontSize, 0.5, 0.5)
	case pick == 2:
		dc.DrawStringAnchored(Text, float64(ImgWidth)/2, float64(ImgHeight)-r.FontSize, 0.5, -18)
	case pick == 3:
		dc.DrawStringWrapped(Text, x, y, 0.5, 0.5, MaxWidth, 1.5, gg.AlignLeft)
	case pick == 4:
		dc.DrawStringWrapped(Text, x, y, 0.5, 0.5, MaxWidth, 1.5, gg.AlignRight)
	case pick == 5:
		dc.DrawStringWrapped(Text, x, y, 0.5, 0.5, MaxWidth, 1.5, gg.AlignCenter)
	default:
		fmt.Println("Invalid Choice")
	}

	dc.SavePNG("out.png")

}
